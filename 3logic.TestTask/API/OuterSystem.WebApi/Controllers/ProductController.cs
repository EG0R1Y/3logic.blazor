﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OuterSystem.Domain.Models;
using OuterSystem.Domain;
using System.Linq;
using System;
using System.Threading.Tasks;
using OuterSystem.Infrastructure;

namespace blazor.app.Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService ?? throw new ArgumentNullException(nameof(productService));
        }

        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            var products = await _productService.GetProductsAsync();
            return Ok(products);
        }

        [HttpPost("Add")]
        public async Task<IActionResult> AddProductAsync([FromBody] Product product)
        {
            await _productService.AddProductAsync(product);

            return new ObjectResult(product) { StatusCode = StatusCodes.Status201Created };
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProductAsync(int id)
        {
            try
            {
                await _productService.DeleteProductAsync(id);
            }
            catch(NotFoundException)
            {
                return NotFound();
            }
            
            return Ok();
        }

        [HttpPut("Change")]
        public async Task<IActionResult> ChangeProductAsync([FromBody] Product product)
        {
            await _productService.UpdateProductAsync(product);

            return new ObjectResult(product) { StatusCode = StatusCodes.Status200OK };
        }
    }
}
