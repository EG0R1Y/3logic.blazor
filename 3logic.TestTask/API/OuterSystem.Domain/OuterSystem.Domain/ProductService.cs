﻿using OuterSystem.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OuterSystem.Domain
{
    ///<inheritdoc/>
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository ?? throw new ArgumentNullException(nameof(productRepository));
        }

        ///<inheritdoc/>
        public Task AddProductAsync(Product product)
            => _productRepository.AddProductAsync(product);

        ///<inheritdoc/>
        public Task DeleteProductAsync(int id)
            => _productRepository.DeleteProductAsync(id);

        ///<inheritdoc/>
        public Task<IEnumerable<Product>> GetProductsAsync()
            => _productRepository.GetProductsAsync();

        ///<inheritdoc/>
        public Task UpdateProductAsync(Product product)
            => _productRepository.UpdateProductAsync(product);
    }
}
