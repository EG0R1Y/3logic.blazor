﻿using OuterSystem.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OuterSystem.Domain
{
    public interface IProductService
    {
        /// <summary>
        /// Добавить товар
        /// </summary>
        /// <param name="product">новый товар</param>
        /// <returns></returns>
        Task AddProductAsync(Product product);

        /// <summary>
        /// Удаление товара по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task DeleteProductAsync(int id);

        /// <summary>
        /// Список товаров
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<Product>> GetProductsAsync();

        /// <summary>
        /// Обновить товар в списке
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        Task UpdateProductAsync(Product product);
    }
}
