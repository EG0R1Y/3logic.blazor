﻿using Microsoft.EntityFrameworkCore;
using OuterSystem.Domain;
using OuterSystem.Domain.Models;
using OuterSystem.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OuterSystem.Infrastructure.Repositories
{
    ///<inheritdoc/>
    public class ProductRepository : IProductRepository
    {
        private readonly ProductContext _productContext;

        public ProductRepository(ProductContext productContext)
        {
            _productContext = productContext ?? throw new ArgumentNullException(nameof(productContext));
        }

        ///<inheritdoc/>
        public async Task AddProductAsync(Product product)
        {
            _productContext.Products.Add(product);
            await _productContext.SaveChangesAsync();
        }

        ///<inheritdoc/>
        public async Task DeleteProductAsync(int id)
        {
            var product = await _productContext.Products.FindAsync(id);

            if (product == null)
            {
                throw new NotFoundException($"Не найден объект с ID={id} для удаления");
            }

            _productContext.Products.Remove(product);
            await _productContext.SaveChangesAsync();
        }

        ///<inheritdoc/>
        public async Task<IEnumerable<Product>> GetProductsAsync()
        {
            return await _productContext.Products.ToListAsync();
        }

        ///<inheritdoc/>
        public async Task UpdateProductAsync(Product product)
        {
            var productOld = await _productContext.Products.FindAsync(product.Id);

            if (productOld == null)
            {
                throw new NotFoundException($"Не найден объект с ID={product.Id} для обновления");
            }

            productOld.Cost = product.Cost;
            productOld.Name = product.Name;
            productOld.Description = product.Description;

            try
            {
                await _productContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException e) when (!ProductItemExists(product.Id))
            {
                throw new NotFoundException($"Не удалось обновить объект с ID={product.Id}", e);
            }
        }

        private bool ProductItemExists(long id) =>
             _productContext.Products.Any(e => e.Id == id);
    }
}
