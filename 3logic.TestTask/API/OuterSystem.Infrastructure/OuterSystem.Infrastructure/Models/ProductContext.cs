﻿using Microsoft.EntityFrameworkCore;
using OuterSystem.Domain.Models;

namespace OuterSystem.Infrastructure.Models
{
    public class ProductContext : DbContext
    {
        public ProductContext(DbContextOptions<ProductContext> options)
            : base(options)
        {
            Database.Migrate();
        }

        public DbSet<Product> Products { get; set; }
    }
}
