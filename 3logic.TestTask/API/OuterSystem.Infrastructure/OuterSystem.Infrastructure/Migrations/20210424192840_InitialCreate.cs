﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OuterSystem.Infrastructure.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Cost = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                });

            migrationBuilder.Sql(@"GO
                            INSERT INTO [dbo].[Products]
                                   ([Name]
                                   ,[Cost]
                                   ,[Description])
                             VALUES
                                   ('Clear Architect'
                                   ,510
                                   ,N'Книга старое издание')
                            GO
                            INSERT INTO [dbo].[Products]
                                   ([Name]
                                   ,[Cost]
                                   ,[Description])
                             VALUES
                                   ('Clear Coder'
                                   ,590
                                   ,N'Книга новое издание')");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Products");
        }
    }
}
