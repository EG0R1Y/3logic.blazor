﻿using Microsoft.AspNetCore.Identity;

namespace blazor.app.Server.Models
{
    public class ApplicationUser : IdentityUser
    {
    }
}
