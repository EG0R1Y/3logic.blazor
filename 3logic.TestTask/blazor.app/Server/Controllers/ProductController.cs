﻿using blazor.app.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace blazor.app.Server.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly ILogger<ProductController> logger;
        private readonly HttpClient _httpClient;

        public ProductController(ILogger<ProductController> logger, IHttpClientFactory clientFactory)
        {
            this.logger = logger;
            _httpClient = clientFactory.CreateClient("client");
        }

        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            var products = await _httpClient.GetFromJsonAsync<List<Product>>("Product");
            return Ok(products);
        }

        [HttpPost("Add")]
        public async Task<IActionResult> AddProductAsync([FromBody] Product product)
        {
            await _httpClient.PostAsJsonAsync("Product/Add", product);

            return new ObjectResult(product) { StatusCode = StatusCodes.Status201Created };
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProductAsync(int id)
        {
            await _httpClient.DeleteAsync($"Product/{id}");
            return Ok();
        }

        [HttpPut("Change")]
        public async Task<IActionResult> ChangeProductAsync([FromBody] Product product)
        {
            await _httpClient.PutAsJsonAsync("Product/Change", product);

            return Ok();
        }
    }
}
