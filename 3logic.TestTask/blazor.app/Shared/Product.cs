﻿using System.ComponentModel.DataAnnotations;

namespace blazor.app.Shared
{
    /// <summary>
    /// Товар
    /// </summary>
    public class Product
    {
        public Product(int id, string name, decimal cost, string description)
        {
            Id = id;
            Name = name;
            Cost = cost;
            Description = description;
        }

        public Product()
        { }

        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название товара
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Стоимость
        /// </summary>
        [Required]
        public decimal Cost { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        [Required]
        public string Description { get; set; }
    }
}
