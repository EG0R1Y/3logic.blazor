﻿using blazor.app.Shared;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace blazor.app.Client.Pages
{
    public partial class Products
    {
        [Inject]
        private HttpClient HttpClient { get; set; }

        private Product[] products;
        private bool isShowAdd = false;
        private bool isShowLoader = false;

        private void ChangeShowAdd()
        {
            isShowAdd = !isShowAdd;
            StateHasChanged();
        }

        private async void UpdateProducts()
        {
            isShowLoader = true;
            StateHasChanged();
            try
            {
                products = await HttpClient.GetFromJsonAsync<Product[]>("Product");
            }
            finally
            {
                isShowLoader = false;
            }

            StateHasChanged();
        }
        protected override async Task OnInitializedAsync()
        {
            isShowLoader = true;
            StateHasChanged();
            try
            {
                products = await HttpClient.GetFromJsonAsync<Product[]>("Product");
            }
            catch (AccessTokenNotAvailableException exception)
            {
                exception.Redirect();
            }
            finally
            {
                isShowLoader = false;
                StateHasChanged();
            }
        }
    }
}
