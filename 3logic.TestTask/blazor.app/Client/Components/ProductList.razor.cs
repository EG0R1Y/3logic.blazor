﻿using blazor.app.Shared;
using Microsoft.AspNetCore.Components;
using System;
using System.Net.Http;

namespace blazor.app.Client.Components
{
    public partial class ProductList
    {
        private int? _selectedProduct = null;
        private bool _showChangeProduct;
        private bool _showDeleteAcceptor;

        [Inject]
        private HttpClient _httpClient { get; set; }

        [Parameter]
        public Product[] Products { get; set; }

        [Parameter]
        public Action UpdateProductsAction { get; set; }

        private void CheckedChanged(int id)
        {
            _selectedProduct = id;
        }

        private void DeleteProduct()
        {
            _showDeleteAcceptor = true;
        }

        private async void AcceptDeleteProduct()
        {
            await _httpClient.DeleteAsync($"Product/{_selectedProduct}");
            UpdateProductsAction?.Invoke();
        }

        private void ChangeProduct()
        {
            _showChangeProduct = true;
        }

        private void CloseModal()
        {
            _showDeleteAcceptor = false;
            _showChangeProduct = false;
            StateHasChanged();
        }
    }
}
