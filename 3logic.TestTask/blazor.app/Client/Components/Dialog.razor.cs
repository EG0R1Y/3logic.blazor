﻿using Microsoft.AspNetCore.Components;
using System;

namespace blazor.app.Client.Components
{
    public partial class Dialog
    {
        [Parameter]
        public Action CloseDialog {get; set; }

        [Parameter]
        public Action AcceptAction { get; set; }

        private void Close()
        {
            CloseDialog?.Invoke();
        }

        private void Accept()
        {
            AcceptAction?.Invoke();
            CloseDialog?.Invoke();
        }
    }
}
