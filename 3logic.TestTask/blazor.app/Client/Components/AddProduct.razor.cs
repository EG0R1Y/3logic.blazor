﻿using blazor.app.Shared;
using Microsoft.AspNetCore.Components;
using System;
using System.Net.Http;
using System.Net.Http.Json;

namespace blazor.app.Client.Components
{
    public partial class AddProduct
    {
        [Inject]
        private HttpClient _httpClient { get; set; }

        [Parameter]
        public bool isEditMode { get; set; } = false;

        [Parameter]
        public Action CloseDialog {get; set; }
        
        [Parameter]
        public Action UpdateProducts { get; set; }

        [Parameter]
        public Product Product { get; set; } = new Product(); 

        private void Close()
        {
            CloseDialog?.Invoke();
        }

        private async void HandleValidSubmit()
        {
            HttpResponseMessage httpResponseMessage;
            if(isEditMode)
            {
                httpResponseMessage = await _httpClient.PutAsJsonAsync("Product/Change", Product);
            }
            else
            {
                httpResponseMessage = await _httpClient.PostAsJsonAsync("Product/Add", Product);
            }

            if(httpResponseMessage.IsSuccessStatusCode)
            {
                UpdateProducts?.Invoke();
            }

            CloseDialog?.Invoke();
        }
    }
}
